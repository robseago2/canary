sudo cp install/mqtt-influxdb-bridge.service /lib/systemd/system/
sudo cp install/canary.service /lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable mqtt-influxdb-bridge
sudo systemctl enable canary
