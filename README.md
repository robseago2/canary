# Canary README #

## Overview ##

![overview](https://bitbucket.org/robseago2/canary/raw/26373dcc2f7f8407334fc9c592d8031cb31f3348/BlockDiag.png)

The software in ths repo is used to build the **canary** binary to read air quality measurements from the Sensirion sgp30, sps30 and scd30 sensors. The values are published to a local **MQTT broker**.
A python script reads the values from the MQTT broker and stores them in an **influxdb database**. **Node-red** monitors MQTT values and generates alarms when thresholds are exceeded. 
**Grafana** is used to graph values retrieved from InfluxDB.

The core of this software came from Sensirion GitHub repos  

*  git clone --recursive https://github.com/Sensirion/embedded-sps.git  
*  git clone --recursive https://github.com/Sensirion/embedded-sgp.git  
*  git clone --recursive https://github.com/Sensirion/embedded-scd.git  

The sensirion drivers, linux i2c and common code were combined into a single folder. The Makefile was adjusted and a main.c file created to support all the sensors in series. The software is intended to run on a Raspberry Pi, but should be able to run on any Linux platform with i2c. MQTT, Node-Red and Grafana all run locally, but could be on an external device.

## Prerequisites ##

* ssh enabled
* MQTT broker https://diyi0t.com/microcontroller-to-raspberry-pi-wifi-mqtt-communication/
* InfluxDB and Grafana https://diyi0t.com/visualize-mqtt-data-with-influxdb-and-grafana/
* Node-Red https://hackaday.com/2020/01/15/automate-your-life-with-node-red-plus-a-dash-of-mqtt/

### Raspberry PI i2c ###
Enable i2c using `sudo raspi-config` and then reboot. Connected i2c devices can be checked using the `i2cdetect -y 1` command.

GPIO connections
Connecting the sensors up to the Pi is documented in gpio.docx the /doc folder

## Software ##

### MQTTInfluxDBBridge.py ###
This python script, subscribes to MQTT and writes to InfluxDB

### Build ###
Run `make` from the canary folder, the **canary** executable is generated in the canary/bin folder

### Install ###
`install.sh` installs and enables the systemd startup script for the canary executable and the MQTT InfluxDB Bridge script

##  Verification ##

* `systemctl status canary` should be active (running)  
* `systemctl status mqtt-influxdb-bridge` should be active (running)  
* Node-Red is on port 1880
* Grafana is on port 3000
* Mosquitto is on port 1883

