/*
 * Copyright (c) 2018, Sensirion AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of Sensirion AG nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <inttypes.h>  // PRIu64
#include <stdio.h>     // printf
#include <unistd.h>    // sleep

#include "sps30.h"
#include "sgp30.h"
#include "scd30.h"

/**
 * TO USE CONSOLE OUTPUT (printf) PLEASE ADAPT TO YOUR PLATFORM:
 * #define printf(...)
 */

/* check float is valid */
static bool isValid(float value)
{
    if(value != value)
        return false;  // nan
    return true;
}

static bool sps30_init(void)
{
    int16_t ret;
    int count = 0;

    /* Busy loop for initialization, because the main loop does not work without
        * a sensor.
        */
    while (sps30_probe() != 0) {
        printf("SPS sensor probing failed\n");
        sensirion_sleep_usec(1000000); /* wait 1s */
        if(++count >= 5) return false;
    }
    printf("SPS sensor probing successful\n");

    uint8_t fw_major;
    uint8_t fw_minor;
    ret = sps30_read_firmware_version(&fw_major, &fw_minor);
    if (ret) {
        printf("error reading firmware version\n");
    } else {
        printf("FW: %u.%u\n", fw_major, fw_minor);
    }

    char serial_number[SPS30_MAX_SERIAL_LEN];
    ret = sps30_get_serial(serial_number);
    if (ret) {
        printf("error reading serial number\n");
    } else {
        printf("Serial Number: %s\n", serial_number);
    }

    ret = sps30_start_measurement();
    if (ret < 0)
        printf("error starting measurement\n");
    printf("measurements started\n");
    return true;
}

static void sps30_loop(void)
{
    struct sps30_measurement m;
    int16_t ret;

    ret = sps30_read_measurement(&m);
    if (ret < 0) {
        printf("error reading sps30 measurement\n");

    } else {
        printf("SPS30 measured values:\n"
                "\t%0.2f pm1.0\n"
                "\t%0.2f pm2.5\n"
                "\t%0.2f pm4.0\n"
                "\t%0.2f pm10.0\n"
                "\t%0.2f nc0.5\n"
                "\t%0.2f nc1.0\n"
                "\t%0.2f nc2.5\n"
                "\t%0.2f nc4.5\n"
                "\t%0.2f nc10.0\n"
                "\t%0.2f typical particle size\n\n",
                m.mc_1p0, m.mc_2p5, m.mc_4p0, m.mc_10p0, m.nc_0p5, m.nc_1p0,
                m.nc_2p5, m.nc_4p0, m.nc_10p0, m.typical_particle_size);

        /* The quick hacky way to do publish to mqtt */
        char cmd[128];
        sprintf(cmd, "mosquitto_pub -t canary/sps30/mass_concentration_010pm -m %4.4f\n", m.mc_1p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/mass_concentration_025pm -m %4.4f\n", m.mc_2p5);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/mass_concentration_040pm -m %4.4f\n", m.mc_4p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/mass_concentration_100pm -m %4.4f\n", m.mc_10p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/number_concentration_005pm -m %4.4f\n", m.nc_0p5);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/number_concentration_010pm -m %4.4f\n", m.nc_1p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/number_concentration_025pm -m %4.4f\n", m.nc_2p5);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/number_concentration_040pm -m %4.4f\n", m.nc_4p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/number_concentration_100pm -m %4.4f\n", m.nc_10p0);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sps30/ave_particle_size -m %4.4f\n", m.typical_particle_size);
        system(cmd);
    }
}

static bool spg30_init(void)
{
    int16_t err;
    uint16_t ethanol_raw_signal, h2_raw_signal;
    int16_t probe;
    int count = 0;

    /* Busy loop for initialization. The main loop does not work without
     * a sensor. */

    while (1) {
        probe = sgp30_probe();

        if (probe == STATUS_OK)
            break;

        if (probe == SGP30_ERR_UNSUPPORTED_FEATURE_SET)
            printf(
                "Your sensor needs at least feature set version 1.0 (0x20)\n");

        printf("SGP sensor probing failed\n");
        sensirion_sleep_usec(1000000);

        if(++count >= 5) return false;
    }

    printf("SGP sensor probing successful\n");

    uint16_t feature_set_version;
    uint8_t product_type;
    err = sgp30_get_feature_set_version(&feature_set_version, &product_type);
    if (err == STATUS_OK) {
        printf("Feature set version: %u\n", feature_set_version);
        printf("Product type: %u\n", product_type);
    } else {
        printf("sgp30_get_feature_set_version failed!\n");
    }
    uint64_t serial_id;
    err = sgp30_get_serial_id(&serial_id);
    if (err == STATUS_OK) {
        printf("SerialID: %" PRIu64 "\n", serial_id);
    } else {
        printf("sgp30_get_serial_id failed!\n");
    }

    /* Read gas raw signals */
    err = sgp30_measure_raw_blocking_read(&ethanol_raw_signal, &h2_raw_signal);
    if (err == STATUS_OK) {
        /* Print ethanol raw signal and h2 raw signal */
        printf("Ethanol raw signal: %u\n", ethanol_raw_signal);
        printf("H2 raw signal: %u\n", h2_raw_signal);
    } else {
        printf("error reading raw signals\n");
    }

    /* Consider the two cases (A) and (B):
     * (A) If no baseline is available or the most recent baseline is more than
     *     one week old, it must discarded. A new baseline is found with
     *     sgp30_iaq_init() */
    err = sgp30_iaq_init();
    if (err == STATUS_OK) {
        printf("sgp30_iaq_init done\n");
    } else {
        printf("sgp30_iaq_init failed!\n");
    }
    /* (B) If a recent baseline is available, set it after sgp30_iaq_init() for
     * faster start-up */
    /* IMPLEMENT: retrieve iaq_baseline from presistent storage;
     * err = sgp30_set_iaq_baseline(iaq_baseline);
     */
    return true;
}

static void spg30_loop(void)
{
    uint16_t i = 0;
    int16_t err;
    uint16_t tvoc_ppb, co2_eq_ppm;
    uint32_t iaq_baseline;

    err = sgp30_measure_iaq_blocking_read(&tvoc_ppb, &co2_eq_ppm);
    if (err == STATUS_OK) {
        printf("SGP30 measured values:\n"
                "\ttVOC  Concentration: %dppb\n"
                "\tCO2eq Concentration: %dppm\n\n",
                tvoc_ppb, co2_eq_ppm);

        /* The quick hacky way to do publish to mqtt */
        char cmd[128];
        sprintf(cmd, "mosquitto_pub -t canary/sgp30/tvoc_concentration -m %d\n", tvoc_ppb);
        system(cmd);
        sprintf(cmd, "mosquitto_pub -t canary/sgp30/co2eq_concentration -m %d\n", co2_eq_ppm);
        system(cmd);
    } else {
        printf("error reading sgp30 IAQ values\n");
    }

    /* Persist the current baseline every hour */
    if (++i % 3600 == 3599) {
        err = sgp30_get_iaq_baseline(&iaq_baseline);
        if (err == STATUS_OK) {
            /* IMPLEMENT: store baseline to presistent storage */
        }
    }
}

static bool scd30_init(void)
{
    int count = 0;
    uint16_t interval_in_seconds = 2;

    /* Busy loop for initialization, because the main loop does not work without
     * a sensor.
     */
    while (scd30_probe() != STATUS_OK) {
        printf("SCD30 sensor probing failed\n");
        sensirion_sleep_usec(1000000u);
        if(++count >= 5) return false;
    }
    printf("SCD30 sensor probing successful\n");

    scd30_set_measurement_interval(interval_in_seconds);
    sensirion_sleep_usec(20000u);
    scd30_start_periodic_measurement(0); // disble ambient pressure compensation
    return true;
}

static void scd30_stop(void)
{
    scd30_stop_periodic_measurement();
}

static void scd30_loop(void)
{
    float co2_ppm, temperature, relative_humidity;
    int16_t err;

    /* Measure co2, temperature and relative humidity and store into
        * variables.
        */
    err = scd30_read_measurement(&co2_ppm, &temperature, &relative_humidity);
    if (err != STATUS_OK) {
        printf("error reading scd30 measurement\n");

    } else {
        printf("SCD30 measured values:\n"
                "\tCO2 Concentration: %0.2f ppm\n"
                "\tTemperature: %0.2f C\n\n"
                "\tRelative Humidity: %0.2f %%\n\n",
                co2_ppm, temperature, relative_humidity);

        /* The quick hacky way to do publish to mqtt */
        char cmd[128];
        if(isValid(co2_ppm)) {        
            sprintf(cmd, "mosquitto_pub -t canary/scd30/co2_concentration -m %0.2f\n", co2_ppm);
            system(cmd);
        }
        if(isValid(temperature)) {
            sprintf(cmd, "mosquitto_pub -t canary/scd30/temperature -m %0.2f\n", temperature);
            system(cmd);
        }
        if(isValid(relative_humidity)) {
            sprintf(cmd, "mosquitto_pub -t canary/scd30/humidity -m %0.2f\n", relative_humidity);
            system(cmd);
        }
    }
}

int main(void)
{
    bool sps30_ok;
    bool spg30_ok;
    bool scd30_ok;

    /* Initialize I2C bus */
    sensirion_i2c_init();

    sps30_ok = sps30_init();
    spg30_ok = spg30_init();
    scd30_ok = scd30_init();

    while (1) {

        sensirion_sleep_usec(SPS30_MEASUREMENT_DURATION_USEC); /* wait 1s */
        if(sps30_ok) sps30_loop();
        if(spg30_ok) spg30_loop();

        sensirion_sleep_usec(SPS30_MEASUREMENT_DURATION_USEC); /* wait 1s */
        if(sps30_ok) sps30_loop();
        if(spg30_ok) spg30_loop();
        if(scd30_ok) scd30_loop();  // max of once every 2 seconds
    }

    scd30_stop();

    return 0;
}
