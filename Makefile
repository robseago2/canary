
CC=g++

CFLAGS=-Os -Wall -Wextra -Wfloat-conversion -Wno-unused-parameter -fstrict-aliasing -Wstrict-aliasing=1 -Wsign-conversion -Wno-unused-result -fPIC
LFLAGS=-Wall -I.

TARGET=canary

SRCDIR=src
OBJDIR=obj
BINDIR=bin

SOURCES:=$(wildcard $(SRCDIR)/*.c)
INCLUDES:=$(wildcard $(SRCDIR)/*.h)
OBJECTS:=$(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
RM=rm -f

.PHONY: all clean

all: $(BINDIR)/$(TARGET)

$(BINDIR)/$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(LFLAGS) -o $@

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) ${OBJECTS}
	$(RM) $(BINDIR)/$(TARGET)
